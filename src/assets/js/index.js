import "@styles/application.scss";


window.addEventListener('click', (ev) => {
    const elm = ev.target;
    const selector = elm.getAttribute('data-target');
    collapse(selector, 'toggle');
}, false);

const fnmap = {
    'toggle': 'toggle',
    'show': 'add',
    'hide': 'remove'
};
const collapse = (selector, cmd) => {
    document.querySelector(selector).classList[fnmap[cmd]]('show');
}

let message = "Hello Webpack";
console.log(` Message is: ${message}`);